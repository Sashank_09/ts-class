package com.db;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Demo4 {
    public static void main(String[] args) {

        Connection connection = DbConnection.getConnection();
        Statement statement = null;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter EmployeeId, EmployeeName, Salary, Gender, EmailId, Password");
        int empId = scanner.nextInt();
        String empName = scanner.next();
        double salary = scanner.nextDouble();
        String gender = scanner.next();
        String emailId = scanner.next();
        String password = scanner.next();

        String insertQuery = "INSERT INTO employee VALUES (" +
                empId + ", '" + empName + "', " + salary + ", '" +
                gender + "', '" + emailId + "', '" + password + "')";

        try {
            statement = connection.createStatement();
            int result = statement.executeUpdate(insertQuery);

            if (result > 0) {
                System.out.println(result + " Record(s) Inserted...");
            } else {
                System.out.println("Record Insertion Failed...");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (connection != null) {
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
