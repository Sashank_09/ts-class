package com.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Demo3 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		int empId = 107;
		double salary = 9845.98;
		
		String updateQuery = "update employee set salary = " + salary + "where empId = " +empId;
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(updateQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) updated...");
			} else {
				System.out.println("Record updation Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}

